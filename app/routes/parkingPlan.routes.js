module.exports = app => {
  const parking = require("../controllers/parkingPlan.controller.js");
  app.post("/createParking", parking.create);//create parking space
  app.get("/findSpots", parking.findAll);//find parking spots
  app.get("/status/:refNum", parking.findOne);//check status for a single slot
  app.get("/overall", parking.aggregate);//overall can be configured for custom cases
  app.put("/maintainace/:fieldType/:refNum", parking.update);//1=>booked 0=>unbooked,//fieldType => 1 : maintainace 2:booked
};
