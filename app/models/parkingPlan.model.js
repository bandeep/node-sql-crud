const sql = require("./db.js");

// constructor
const parkingPlan = function(parkingPlan) {
  this.booked = parkingPlan.booked;
  this.refNum = parkingPlan.refNum;
  this.maintainance = parkingPlan.maintainance;
  this.timeOfParking = parkingPlan.timeOfParking;
};

parkingPlan.create = (newparkingPlan, result) => {
    sql.query("INSERT INTO parkingPlan SET ?", newparkingPlan, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }

      console.log("created parkingPlan: ", { id: res.insertId, ...newparkingPlan });
      result(null, { id: res.insertId, ...newparkingPlan });
    });
};

parkingPlan.createAll = (newparkingPlan, result) => {
  for(let itr =0;itr < newparkingPlan.length;itr++){
    newparkingPlan[itr]['timeOfParking'] = new Date();
    console.log("date,",newparkingPlan[itr]['timeOfParking']);
    sql.query("INSERT INTO parkingPlan SET ?", newparkingPlan[itr], (err, res) => {
      if (err) {
        console.log("error: ", err);
        return;
      }
      console.log("created parkingPlan: ", { id: res.insertId, ...newparkingPlan[itr] });
    });
  }
  result(null, {'msg':'create all successfully'});
};

parkingPlan.findById = (parkingPlanId,rate,result) => {
  sql.query(`SELECT * FROM parkingPlan WHERE refNum = ${parkingPlanId} AND booked = 1`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      let diffInMilliSeconds = Math.abs(new Date() - new Date(res[0].timeOfParking)) / 1000;
      const hours = Math.floor(diffInMilliSeconds / 3600) % 24;
      diffInMilliSeconds -= hours * 3600;
      console.log("found parkingPlan: ", (hours + 1) * rate);
      result(null, {'body':(hours + 1) * rate});
      return;
    }
    result({ kind: "not_found" },{'body':diffInMilliSeconds * rate});
  });
};

parkingPlan.getAll = (query,result) => {
  sql.query("SELECT * FROM parkingPlan " + query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log("parkingPlan: ", res);
    result(null, res);
  });
};

parkingPlan.updateById = (fieldType,id, parkingPlan, result) => {
  let field = fieldType == 1 ? 'maintainance' : 'booked';
  sql.query(
    `UPDATE parkingPlan SET ${field} = ${parkingPlan.status} WHERE refNum = ${id}`,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found parkingPlan with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated parkingPlan: ", { id: id, ...parkingPlan });
      result(null, { id: id, ...parkingPlan });
    }
  );
};

parkingPlan.remove = (id, result) => {
  sql.query("DELETE FROM parkingPlan WHERE id = ?", id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    if (res.affectedRows == 0) {
      // not found parkingPlan with the id
      result({ kind: "not_found" }, null);
      return;
    }

    console.log("deleted parkingPlan with id: ", id);
    result(null, res);
  });
};

parkingPlan.removeAll = result => {
  sql.query("DELETE FROM parkingPlan", (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
      return;
    }

    console.log(`deleted ${res.affectedRows} parkingPlan`);
    result(null, res);
  });
};

parkingPlan.custQuery = (query,result) => {
  sql.query(`SELECT * FROM parkingPlan WHERE` + query, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found parkingPlan: ", res[0]);
      result(null, res[0]);
      return;
    }
    result({ kind: "not_found" }, null);
  });
};

parkingPlan.aggregate = (query,result) => {
  sql.query(`SELECT * FROM parkingPlan`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found parkingPlan: ", res[0]);
      result(null, res);
      return;
    }
    result({ kind: "not_found" }, null);
  });
};

module.exports = parkingPlan;
