const parkingPlan = require("../models/parkingPlan.model.js");
exports.create = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  let data = req.body;
      parkingPlan.createAll(data['slotsToMake'], (err, data) => {
        if (err)
        res.status(500).send({
          message:
          err.message || "Some error occurred while creating the parkingPlan."
        });
        else res.send(data);
      });
};
exports.findAll = (req, res) => {
  parkingPlan.getAll('WHERE booked = 0 AND maintainance = 0 ORDER BY refNum*1 ASC LIMIT 1;',(err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving customers."
      });
    else res.send(data);
  });
};


exports.aggregate = (req, res) => {
  parkingPlan.aggregate('',(err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving customers."
      });
    else res.send(data);
  });
};

// Find a single parkingPlan with a customerId
exports.findOne = (req, res) => {
  parkingPlan.findById(req.params.refNum,10, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found parkingPlan with id ${req.params.refNum}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving parkingPlan with id " + req.params.v
        });
      }
    } else res.send(data);
  });
};

// Update a parkingPlan identified by the customerId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  console.log(req.body);

  parkingPlan.updateById(
    req.params.fieldType,
    req.params.refNum,
    req.body,
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found parkingPlan with id ${req.params.customerId}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating parkingPlan with id " + req.params.customerId
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a parkingPlan with the specified customerId in the request
exports.delete = (req, res) => {
  parkingPlan.remove(req.params.customerId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found parkingPlan with id ${req.params.customerId}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete parkingPlan with id " + req.params.customerId
        });
      }
    } else res.send({ message: `parkingPlan was deleted successfully!` });
  });
};

// Delete all parkingPlans from the database.
exports.deleteAll = (req, res) => {
  parkingPlan.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all customers."
      });
    else res.send({ message: `All parkingPlans were deleted successfully!` });
  });
};
